table! {
    month_names (id_month_name) {
        id_month_name -> Int4,
        name -> Varchar,
    }
}

table! {
    pause_times (id_pause) {
        id_pause -> Int4,
        start -> Nullable<Timestamp>,
        stop -> Nullable<Timestamp>,
        workday_id -> Int4,
    }
}

table! {
    workdays (id_workday) {
        id_workday -> Int4,
        start -> Nullable<Timestamp>,
        stop -> Nullable<Timestamp>,
        workmonth_id -> Int4,
    }
}

table! {
    workmonths (id_workmonth) {
        id_workmonth -> Int4,
        month_name_id -> Int4,
    }
}

joinable!(pause_times -> workdays (workday_id));
joinable!(workdays -> workmonths (workmonth_id));
joinable!(workmonths -> month_names (month_name_id));

allow_tables_to_appear_in_same_query!(month_names, pause_times, workdays, workmonths,);
