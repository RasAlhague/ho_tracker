use super::schema::pause_times;
use super::schema::workdays;
use super::schema::workmonths;
use chrono::NaiveDateTime;

#[derive(Queryable)]
pub struct MonthName {
    pub id_month_name: i32,
    pub month_name: String,
}

#[derive(Queryable)]
pub struct PauseTime {
    pub id_pause: i32,
    pub start: Option<NaiveDateTime>,
    pub stop: Option<NaiveDateTime>,
    pub workday_id: i32,
}

#[derive(Insertable)]
#[table_name = "pause_times"]
pub struct NewPauseTime {
    pub start: Option<NaiveDateTime>,
    pub stop: Option<NaiveDateTime>,
    pub workday_id: i32,
}

#[derive(Queryable)]
pub struct WorkMonth {
    pub id_workmonth: i32,
    pub month_name_id: i32,
}

#[derive(Insertable)]
#[table_name = "workmonths"]
pub struct NewWorkMonth {
    pub month_name_id: i32,
}

#[derive(Queryable)]
pub struct WorkDay {
    pub id_workday: i32,
    pub start: Option<NaiveDateTime>,
    pub stop: Option<NaiveDateTime>,
    pub workmonth_id: i32,
}

#[derive(Insertable)]
#[table_name = "workdays"]
pub struct NewWorkDay {
    pub start: Option<NaiveDateTime>,
    pub stop: Option<NaiveDateTime>,
    pub workmonth_id: i32,
}
