DROP TABLE IF EXISTS pause_times;
DROP TABLE IF EXISTS workdays;
DROP TABLE IF EXISTS workmonths;
DROP TABLE IF EXISTS month_names;

CREATE TABLE IF NOT EXISTS month_names (
    id_month_name SERIAL PRIMARY KEY,
    name VARCHAR(20) NOT NULL
); 

CREATE TABLE IF NOT EXISTS workmonths (
    id_workmonth SERIAL PRIMARY KEY,
    month_name_id INT NOT NULL,
    FOREIGN KEY (month_name_id) REFERENCES month_names(id_month_name)
);

CREATE TABLE IF NOT EXISTS workdays (
    id_workday SERIAL PRIMARY KEY,
    start TIMESTAMP NOT NULL,
    stop TIMESTAMP NOT NULL,
    workmonth_id INT NOT NULL,
    FOREIGN KEY (workmonth_id) REFERENCES workmonths(id_workmonth)
);

CREATE table IF NOT EXISTS pause_times (
    id_pause SERIAL PRIMARY KEY,
    start TIMESTAMP NOT NULL,
    stop TIMESTAMP NOT NULL,
    workday_id INT NOT NULL,
    FOREIGN KEY (workday_id) REFERENCES workdays(id_workday)
);